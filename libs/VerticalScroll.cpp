#include <Arduboy2.h>
Arduboy2 arduboy;
void verticalScroll(x,y,w,h,px) {
	for (int xi = x;xi<x+w;x++) {
		for (int yi = y+px;yi<y+h;y++) {
			arduboy.drawPixel(xi,yi-px,arduboy.getPixel(xi,yi));
		}
		for (int yi = y+(h-px);yi<y+h;y++) {
			arduboy.drawPixel(xi,yi,BLACK);
		}
	}
}